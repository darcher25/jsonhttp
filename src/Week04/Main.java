package Week04;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;
import java.util.Map;

/*JsonProcessingException is for all problems when processing JSON
input or output that are not pure IO exceptions.*/

public class Main {
    public static String employeeToJSON(Employee employee) {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try{
            /*writeValueAsString is method that takes an object
            and returns JSON*/
            s = mapper.writeValueAsString(employee);
        }catch (JsonProcessingException e) {
            //err will give a detailed reason for thrown exception
            System.err.println(e.toString());
        }
        return s;

    }

    public static Employee JsonToEmployee(String s) {
        ObjectMapper mapper = new ObjectMapper();
        Employee employee = null;

        try{
            employee = mapper.readValue(s, Employee.class);
        }catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return employee;
    }

    public static void main(String[] args) {
        Employee staff = new Employee();
        staff.setName("Billy");
        staff.setTitle("Data Clerk");
        staff.setWage(15);

        String example = Main.employeeToJSON(staff);
        System.out.println(example);

        Employee newStaff = Main.JsonToEmployee(example);
        System.out.println(newStaff);

        System.out.println("What is the average pay for this job title?");
        System.out.println("Check out the webpage: ");
        System.out.println("https://www.salary.com/research/salary/benchmark/data-entry");

        System.out.println(HTTPGetter.getHTTP("https://www.salary.com/research/salary/benchmark/data-entry" +
                "-clerk-i-salary"));

        Map<Integer, List<String>> map = HTTPGetter.getHttpHeaders("https://www.salary" +
                ".com/research/salary/benchmark" +
                "/data-entry\n" +
                "-clerk-i-salary");

        for (Map.Entry<Integer, List<String>> entry : map.entrySet()) {
            try {
                System.out.println("Key= " + entry.getKey() +
                        entry.getValue());

            } catch (Exception e) {
                System.err.println(e.toString());
            }

        }
    }
}
