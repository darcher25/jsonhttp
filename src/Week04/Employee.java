package Week04;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Employee {

    private String name;
    private String title;
    private double wage;
    private NumberFormat format = new DecimalFormat("#0.00");

    //getters & setters for variables
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public double getWage() {
        return wage;
    }
    public void setWage(long wage) {
        this.wage = wage;
    }

    //use the toString method for formatting
    public String toString() {
        return "Name: " + name  + "\nTitle: " + title + "\nWage per hour: $" + format.format(wage);
    }
}
