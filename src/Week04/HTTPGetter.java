package Week04;

import javax.swing.text.AbstractDocument;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class HTTPGetter {
    public static String getHTTP(String s) {
        String httpContent= "";

        try {
            URL url = new URL(s);
            StringBuilder stringBuilder = new StringBuilder();

            //class to est connection
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            //BufferedReader-Reads text from a character-input stream
            //InputStreamReader-Creates an InputStreamReader that uses the default charset
            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));

            String eachLine = null;
            while ((eachLine = reader.readLine()) !=null) {
                stringBuilder.append(eachLine + "\n");
            }
            httpContent = stringBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }
        return httpContent;
    }

    public static Map getHttpHeaders(String s) {
        /*
        An HTTP header consists of its case-insensitive name
        followed by a colon (:), then by its value.
        Whitespace before the value is ignored.
         */

        Map hashMap = null;
        try {
            URL url = new URL(s);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            hashMap = http.getHeaderFields();
        } catch (Exception e) {
            System.err.println(e.toString());
        }
        return hashMap;
    }

}
